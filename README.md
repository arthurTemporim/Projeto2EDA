# Decodificação de mensagem usando Lista e Árvore

Este Projeto contém um programa feito em **C** capaz de decodificar uma mensagem feita com um *código morse* genérico usando **Lista** e **Árvore AVL**.

Por fim a mensagem decodificada é apresentada, e o tempo de execução para cada decodificação é apresentado para fins de comparação.

## Play

### Compila

> `make`

### Executa

> `make run`

### Limpa o binário

> `make clean`
