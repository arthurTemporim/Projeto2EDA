#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//------------------------------------LISTA---------------------//
typedef struct elemento {
	char dado;
	char * codigo;
	struct elemento * proximo;
}elemento;

typedef struct lista {
	elemento * inicio;
	elemento * fim;
}lista;

//Lista global, para manipular as listas em todas as funções.
lista * global;

//Função para inserir elementos no "início"?? da lista.
void insereLista(char letra,char * codigo, lista * l) {

	elemento * novoElemento = (elemento *) malloc(sizeof(elemento));
	novoElemento->dado = letra;
	novoElemento->proximo=NULL;
	novoElemento->codigo=codigo;
	if(l->fim == NULL) {
		l->inicio=novoElemento;
		l->fim = novoElemento;
	}else{
		l->fim->proximo = novoElemento;
		l->fim = novoElemento;
	}
	//printf("%s\n",l->fim->codigo );
}

//Removendo do "fim"?? da lista.
char removeLista(lista * l){
	char tmp;
    if(l->fim == NULL){
		return EOF;
    }else if(l->inicio == l->fim){
       tmp = l->inicio->dado;
       free(l->inicio);
       l->inicio == NULL;
       l->fim = NULL;  
    }else{
 	   	elemento * penultimo = l->inicio;
 	   	l->inicio = penultimo->proximo;
    	tmp = penultimo->dado;
    	free(penultimo);
	}
    return tmp;
}

//Insere o conteúdo do arquivo, de forma organisada em uma lista.
void arquivoLista(char * nomeArquivo) {

	FILE * arquivo;
	char codigo;

	lista * novaLista = (lista *) malloc(sizeof(lista));	
	char * auxiliar = (char *) malloc(sizeof(char));
	arquivo = fopen(nomeArquivo, "r");

	do {
		codigo = fgetc(arquivo);
		if(codigo == '\n') {
			codigo = ' ';
		}
	insereLista(codigo,auxiliar, novaLista);	
	}while(codigo != EOF);

	fclose(arquivo);

//TESTA LISTA!
/*
	elemento * aux=novaLista->inicio;
	printf("\n\n\nteste da lista\n\n\n");
	do {
		codigo =aux->dado;
		aux=aux->proximo;
		printf("%c", codigo);
	} while (codigo != EOF);
*/
	
	global->inicio = novaLista->inicio;
	global->fim = novaLista->fim;
}

//Função para decodificar mensagem.

void comparaCodigo(lista * codigoLetra, char * letra, lista * listaAux) {
	int i=0;
	int contaIgual=0;
	int tamanho = strlen(letra);
	elemento * percorreLista= codigoLetra->inicio;
	while (percorreLista != NULL) {
		
			if(!strcmp(percorreLista->codigo,letra)) {
				printf("%c",  percorreLista->dado);
			}
		percorreLista = percorreLista->proximo;
	}
	/*
	if(contaIgual == tamanho) {
		return listaAux->inicio->dado;
	}*/
		//return ' ';
}

void decodificaMensagemLista(lista * codigoLetra) {

	FILE * mensagem;
	lista * listaAux = (lista*) malloc(sizeof(lista));

	listaAux->inicio = codigoLetra->inicio;
	listaAux->fim = codigoLetra->fim;

	char aux;
	char codigo[15];
	int i;
	
	mensagem = fopen("doc/mensagem.txt", "r");

	do {
		i=0;
		do {
			aux = fgetc(mensagem);
			if(aux == '/'){
				printf(" ");
			}
			if(aux == EOF || aux == ' '|| aux == '/')
				break;
			codigo[i++] = aux;
		} while (aux != ' ');
		codigo[i]='\0';
		//printf("%s\n", codigo);
		//printf("%c\n", aux);
		comparaCodigo(codigoLetra, codigo, listaAux);
	} while (aux != EOF);

	fclose(mensagem);
}


//Cria a lista para fazer a comparação com a árvore.
lista * criaLista(){
	FILE * arquivo;
	char letra;
	int i;

	arquivo = fopen("doc/morse.txt","r"); 
	char  * codigo = (char * )malloc(sizeof(char));
	lista * novaLista = (lista *) malloc(sizeof(lista));
	novaLista->inicio = NULL;
	novaLista->fim= NULL;
//	elemento * auxLista;
	while(fscanf(arquivo,"%c %s",&letra,codigo)!=EOF){
		//printf("oi\n");
		fgetc(arquivo);
		insereLista(letra,codigo,novaLista);
		codigo=(char *) malloc(sizeof(char));
		//printf("%p\n", novaLista->fim->proximo);		
		//insereLista(letra, " ", novaLista);
		//printf("%c", novaLista->fim->dado);
		
//Imprime Lista
		//i=0;
		
		 
	// 	auxLista = novaLista->fim;
	// printf("%s\n",auxLista->codigo );
		// while(auxLista != NULL){
		// 	printf("%p ", auxLista);
		// 	auxLista = auxLista->proximo;
		// }	
	}

	     
	// printf("saiu da funcao\n");
	fclose(arquivo);
	return novaLista;
}

//------------------------------------LISTA---------------------//

//Struct da Árvore
typedef struct arvore {
	char dado;
	struct arvore * noEsquerda;
	struct arvore * noDireita;
}no;

//Função para criação de novo nó.
no * criaNovoNo() {
	return (no *) malloc(sizeof(no));
}

//Função para inserir o conteúdo do arquivo na árvore.
void insereDado(char letra, FILE * arquivo, no * raiz) {
	char aux;
	aux= fgetc(arquivo);
	if(aux == '-') {
		if(raiz->noDireita == NULL) {
			raiz->noDireita = criaNovoNo();
		}
		return insereDado(letra,arquivo, raiz->noDireita);		
	} else if(aux == '.') {
		if(raiz->noEsquerda == NULL) {
			raiz->noEsquerda = criaNovoNo();
		}
		return insereDado(letra, arquivo, raiz->noEsquerda);
	} else if(aux=='\n') {
		raiz->dado = letra;
	}
}

//Função que separa o caractere do resto do arquivo.
void insereArquivoLista(FILE * arquivo, no * raiz) {
	
	char letra;
	char caractere;

	letra = fgetc(arquivo);
	fgetc(arquivo);

	//printf("%c ", letra);
	
	if(letra != EOF) {
		insereDado(letra, arquivo, raiz);
		return insereArquivoLista(arquivo, raiz);
	}
}

//Função para decodigicar a mensagem, ja em uma lista.
char decodificaMensagem(lista * mensagem, no * raiz,no * raizPrincipal) {

	char operador;
	
	do {
		operador = removeLista(mensagem);
		if(operador == '.') {
			raiz = raiz->noEsquerda;
		} else if(operador == '-') {
			raiz = raiz->noDireita;
		}
	}while(operador != ' ' && operador!='/' && operador!= EOF);
	if(operador==' ')
		return raiz->dado;
	else if(operador == EOF)
		return EOF;
	else 
		return ' ';
}

//Função para imprimir a mensagem decodificada.
void imprimeMensagem(lista * mensagem, no * raiz,no * raizPrincipal) {
	char operador;
	
	do {
		operador = decodificaMensagem(mensagem,raizPrincipal,raizPrincipal);
//IMPRIME A MENSAGEM.
		if(operador != EOF) 
			printf("%c", operador);
	}while( operador!= EOF);
}

int main() {
	clock_t inicio,fim;
	double tempoArvore,tempoLista;
	global = (lista*) malloc(sizeof(lista));
	no * raiz = criaNovoNo();
	
	FILE * arquivo;
	arquivo = fopen("doc/morse.txt","r"); 
	
	insereArquivoLista(arquivo, raiz);

	arquivoLista("doc/mensagem.txt");
	inicio=clock();
	imprimeMensagem(global,raiz,raiz);
	fim=clock();
	tempoArvore = (double) (fim-inicio)/CLOCKS_PER_SEC;
	printf("\ntempo de decodificar arvore: %lf s\n",tempoArvore);
		
	lista * codigoLetra = criaLista();
//printf("oi\n");
	// printf("%s\n", codigoLetra->inicio->codigo);
	inicio = clock();
	decodificaMensagemLista(codigoLetra);
	fim= clock();
	tempoLista=(double) (fim-inicio)/CLOCKS_PER_SEC;
	printf("\ntempo de decodificar lista: %lf s\n",tempoLista);
	fclose(arquivo);
	return 0;
}
